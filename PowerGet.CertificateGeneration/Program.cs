﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;
using System.Security.Cryptography.X509Certificates;

namespace PowerGet.CertificateGeneration
{
    class Program
    {
        static void Main(string[] args)
        {
            const string password = "hardcodedpassword";
            var keygen = new RsaKeyPairGenerator();
            keygen.Init(new KeyGenerationParameters(new SecureRandom(), 2048));

            var keys = keygen.GenerateKeyPair();

            var certGen = new X509V3CertificateGenerator();
            var dnName = new X509Name("CN=DO NOT TRUST PowerGet Local Self-Signed Codesigning Cert");

            certGen.SetSerialNumber(BigInteger.ValueOf(1));
            certGen.SetIssuerDN(dnName);
            certGen.SetNotBefore(DateTime.Today);
            certGen.SetNotAfter(DateTime.Today.AddYears(1));
            certGen.SetSubjectDN(dnName);
            certGen.SetPublicKey(keys.Public);
            var usages = new[] { KeyPurposeID.IdKPCodeSigning };
            certGen.AddExtension(X509Extensions.ExtendedKeyUsage.Id, false, new ExtendedKeyUsage(usages));
            certGen.SetSignatureAlgorithm("SHA1WITHRSA");


            // Self-signed, so it's all the same.
            var issuerKeyPair = keys;
            var issuerDN = dnName;
            var issuerSerialNumber = BigInteger.ValueOf(1);

            var authorityKeyIdentifier =
                new AuthorityKeyIdentifier(
                    SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(keys.Public),
                    new GeneralNames(new GeneralName(issuerDN)),
                    issuerSerialNumber);
            certGen.AddExtension(
                X509Extensions.AuthorityKeyIdentifier.Id, false, authorityKeyIdentifier);
            certGen.AddExtension(
    X509Extensions.BasicConstraints.Id, true, new BasicConstraints(false));
            var certificate = certGen.Generate(keys.Private);
            //https://blog.differentpla.net/post/20
            var store = new Pkcs12Store();
            string friendlyName = certificate.SubjectDN.ToString();
            var certificateEntry = new X509CertificateEntry(certificate);
            store.SetCertificateEntry(friendlyName, certificateEntry);


            store.SetKeyEntry(friendlyName, new AsymmetricKeyEntry(keys.Private), new[] { certificateEntry });
            var stream = new MemoryStream();
            store.Save(stream, password.ToCharArray(), new SecureRandom());
            File.WriteAllBytes("selfsigned-codesigning.pfx", stream.ToArray());
            var convertedCertificate =
        new X509Certificate2(stream.ToArray(),
                             password,
                             X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);

            var myCerts = new X509Store(StoreName.My);
            myCerts.Open(OpenFlags.ReadWrite);
            myCerts.Add(convertedCertificate);

          
        }
    }
}
